package main

import "fmt"

func main() {
	var n int
	fmt.Scan(&n)
	fb := fibanachinumber(n)
	fmt.Println(fb)
}

func fibanachinumber(n int) int {
	x := 0
	y := 1
	if n <= 1 {
		return n
	}
	for i := 1; i < n; i++ {
		x, y = y, x+y
	}
	return x
}
